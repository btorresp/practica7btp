package com.example.practica7btp.model

data class Contact(var name: String, var age: Int, var email: String)