package com.example.practica7btp.widget

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.practica7btp.R
import com.example.practica7btp.model.Contact

class ContactViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_contact,parent,false )){

    var txtName: TextView
    var txtAge: TextView
    var txtEmail: TextView

    init {
        txtName = itemView.findViewById(R.id.txtName)
        txtAge = itemView.findViewById(R.id.txtAge)
        txtEmail = itemView.findViewById(R.id.txtEmail)
    }

    fun bind(contact : Contact){
        txtName.text = contact.name
        txtAge.text = contact.age.toString()
        txtEmail.text= contact.email
    }
}